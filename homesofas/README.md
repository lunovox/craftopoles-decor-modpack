![screenshot1]

# [[HOME SOFAS]]

[Minetest Mod] Adds several colorful sofas.

**Dependencies:**
  * default → Minetest Game Included
  * wool → Minetest Game Included
  * dye → Minetest Game Included

**Optional Dependencies:**
  * [intllib] → Facilitates the translation of several other mods into your native language, or other languages.
  * [tradelands] → Protection of your lands and your objects like this home sofas. 

**Licence:**
 * GNU AGPL: https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License

**Developers:**
 * Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](http:mastodon.social/@lunovox), [webchat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [Mumble](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

**Recipe:**
![recipe1] ![recipe2]  ![recipe3]

-----

[HOME SOFAS]:https://gitlab.com/lunovox/craftopoles-decor-modpack/-/tree/master/homesofas
[intllib]:https://github.com/minetest-mods/intllib
[recipe1]:https://gitlab.com/lunovox/craftopoles-decor-modpack/-/raw/master/homesofas/screenshots/img_recipe1.png
[recipe2]:https://gitlab.com/lunovox/craftopoles-decor-modpack/-/raw/master/homesofas/screenshots/img_recipe2.png
[recipe3]:https://gitlab.com/lunovox/craftopoles-decor-modpack/-/raw/master/homesofas/screenshots/img_recipe3.png
[screenshot1]:https://gitlab.com/lunovox/craftopoles-decor-modpack/-/raw/master/homesofas/screenshots/img_screenshot1.png
[tradelands]:https://gitlab.com/lunovox/tradelands

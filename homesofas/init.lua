local modName = minetest.get_current_modname()
local modPath = minetest.get_modpath(minetest.get_current_modname())

dofile(modPath.."/api.lua")

homesofas.sofas_list = {
	{"red",		"Red Sofa", },
	{"orange",	"Orange Sofa", },
	{"yellow",	"Yellow Sofa", },
	{"green",	"Green Sofa", },
	{"blue",		"Blue Sofa", },
	{"violet",	"Violet Sofa", },
	{"black",	"Black Sofa", },
	{"grey",		"Grey Sofa", },
	{"white",	"White Sofa", },
}

for i in ipairs(homesofas.sofas_list) do
	homesofas.register_sofa1seat(homesofas.sofas_list[i][1], {
		description = homesofas.sofas_list[i][2].." (1 seat)",
	})
	homesofas.register_sofa2seats(homesofas.sofas_list[i][1], {
		description = homesofas.sofas_list[i][2].." (2 seats)",
	})
	homesofas.register_sofa3seats(homesofas.sofas_list[i][1], {
		description = homesofas.sofas_list[i][2].." (3 seats)",
	})
end

minetest.log("action", "["..modName:upper().."] Loaded!")

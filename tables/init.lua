local function registered_nodes_by_group(group)
	local result = {}
	for name, def in pairs(minetest.registered_nodes) do
		if def.groups[group] then
			result[#result+1] = def
		end
	end
	return result
end

local woodtypes = registered_nodes_by_group("wood")

for i = 1, #woodtypes do
	local name = woodtypes[i].name:split(":")[2]
	local ingredient = woodtypes[i].name
	if name == "wood" then name = "apple_wood" end
	if name == "junglewood" then name = "jungle_wood" end
	
	minetest.register_node("tables:" .. name .. "_table", {
	description = string.sub(woodtypes[i].description, 1, #woodtypes[i].description - 8) .. "Table",
	tiles = {woodtypes[i].tiles[1]},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-1/8, -0.5, -1/8, 1/8, 3/8, 1/8},
			{-0.5, 3/8, -0.5, 0.5, 0.5, 0.5}
		},
	},
	groups = {choppy = 2, oddly_breakable_by_hand = 2},
	sounds = default.node_sound_wood_defaults()
	})

	minetest.register_craft( {
	output = "tables:" .. name .. "_table 5",
	recipe = {
		{ingredient, ingredient, ingredient},
		{"", ingredient, ""},
		{"", ingredient, ""},
	}
})
end
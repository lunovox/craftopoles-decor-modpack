local px = 1/32
doorroller.node_format = {
	opened = function(height)
	   if type(height)~="number" then
	      height = 3
      end
   	return {
   		--rolo
   		{
   		   -px*16,px*64 + (height-3),-px*3, 
   		   px*16,px*80 + (height-3),-px*11
   		}, 	--ok
   		{
   		   -px*16,px*68 + (height-3),px, 
   		   px*16,px*76 + (height-3),-px*15
   		},		--ok
   		{
   		   -px*16,px*67 + (height-3),px*-1, 
   		   px*16,px*77 + (height-3),-px*13
   		},	--ok
   		{
   		   -px*16,px*66 + (height-3),px*-2, 
   		   px*16,px*78 + (height-3),-px*12
   		},	--ok
   		
   		 --porta aberta
   		{
   		   -px*16,px*64 + (height-3),-px*14, 
   		   px*16,px*76 + (height-3),-px*15
   		},
   	}
   end,
	closed = function(height)
	   if type(height)~="number" then
	      height = 3
      end
   	return {
   		--rolo
   		{
   		   -px*16,px*64 + (height-3),-px*3, 
   		   px*16,px*80 + (height-3),-px*11
   		}, 	--ok
   		{
   		   -px*16,px*68 + (height-3),px, 
   		   px*16,px*76 + (height-3),-px*15
   		},		--ok
   		{
   		   -px*16,px*67 + (height-3),px*-1, 
   		   px*16,px*77 + (height-3),-px*13
   		},	--ok
   		{
   		   -px*16,px*66 + (height-3),px*-2, 
   		   px*16,px*78 + (height-3),-px*12
   		},	--ok
   		
   		 --porta fechada
   		{
   		   -px*16,-px*16,-px*14, 
   		   px*16,px*76 + (height-3),-px*15
   		},		
   	}
   end,
}
doorroller.tiles = { -- Top, base, right, left, front, back
	"tex_door_roller_from.png", --top
	"tex_door_roller_from.png", --base
	"tex_door_roller_side.png", --right
	"tex_door_roller_side.png^[transformFX", --left
	"tex_door_roller_from.png", --from
	"tex_door_roller_from.png", --back
}
doorroller.sounds = { --need mod default of minetets_game gamepack
	default = {
		dug = {
			name = "default_dug_metal",
			gain = 0.5
		},
		dig = {
			name = "default_dig_metal",
			gain = 0.5
		},
		footstep = {
			name = "default_metal_footstep",
			gain = 0.4
		},
		place = {
			name = "default_place_node_metal",
			gain = 0.5
		}
	},
	open = "doors_steel_door_open",
	close = "doors_steel_door_close"
}
doorroller.doSound = function(pos, soundname) --sample: doorroller.doSound(pos, doorroller.sounds.close)
	minetest.sound_play(soundname,	{pos = pos, gain = 0.3, max_hear_distance = 10}, true)
end

doorroller.on_place = function(itemstack, user, pointed_thing, height)	
	if minetest.is_protected(pointed_thing.above,user:get_player_name()) then
		return itemstack
	end
	if type(height)~="number" then
      height = 3
   end
	local username = user:get_player_name()
	local px = {
	   x = tonumber(pointed_thing.above.x),
	   y = tonumber(pointed_thing.above.y),
	   z = tonumber(pointed_thing.above.z),
	}
	local base = tonumber(pointed_thing.above.y)
	local blocked = false
	for i=1, height, 1 do 
	   px.y = base + (i - 1)
	   local nodex = minetest.get_node(px)
	   --minetest.chat_send_player(username, "i="..i.." pointed_thing.above.y="..pointed_thing.above.y.." px.y="..dump(px.y).." nodex.name="..nodex.name)
	   if nodex.name ~= "air" then
	      blocked = true
	      break
      end
	end
	if not blocked then
		local dir=minetest.dir_to_facedir(user:get_look_dir()) --dir = 0:NORTE 1:OESTE 2:SUL 3:LESTE
		minetest.set_node(pointed_thing.above, {name = "doorroller:doorroller_"..height.."y_closed",param1="",param2=dir})
		if not minetest.is_creative_enabled(username) then
			itemstack:take_item()
		end
		local meta = minetest.get_meta(pointed_thing.above)
		meta:set_string("owner", username)
		meta:set_string("infotext", "Roller Door (Owned by '"..username.."')")
	else
		minetest.chat_send_player(username, minetest.colorize("#FF0000", "[ROLLER DOOR]").." "
			..doorroller.translate("Clear the area before installing the Roller Door!")
		)
	end
	return itemstack
end

doorroller.canIntect=function(pos, playername)
	local meta = minetest.get_meta(pos)
	if 
		meta:get_string("owner")==playername 
		or (
			minetest.get_modpath("tradelands") 
			and modTradeLands.getOwnerName(pos)~="" 
			and modTradeLands.canInteract(pos, playername)
		) or (
			minetest.get_modpath("areas") 
			and #areas:getNodeOwners(pos)>=1 
			and areas:canInteract(pos, playername)
		)
	then
		return true
	end
	return false
end

doorroller.on_rightclick = function(pos, node, clicker, itemstack, pointed_thing, height)
	local username = clicker:get_player_name()
	local meta = minetest.get_meta(pos)
	local owner = meta:get_string("owner")
	if type(height)~="number" then
      height = 3
   end
	if doorroller.canIntect(pos, username) then
	--if doorroller.can_dig(pos, clicker) then
		local node = minetest.get_node(pos)
		local status
		if node.name == "doorroller:doorroller_"..height.."y_opened" then
			status = "open"
			--minetest.chat_send_player(username, minetest.colorize("#00FF00", "[ROLLER DOOR]").." Fechado!")
			doorroller.doSound(pos, doorroller.sounds.close)
		elseif node.name == "doorroller:doorroller_"..height.."y_closed" then
			status = "close"
			--minetest.chat_send_player(username, minetest.colorize("#00FF00", "[ROLLER DOOR]").." Aberto!")
			doorroller.doSound(pos, doorroller.sounds.open)
		end
		local dir = node.param2
		
		if status == "open" then
			minetest.set_node(pos, {name = "doorroller:doorroller_"..height.."y_closed",param1="",param2=dir})
		elseif status == "close" then
			minetest.set_node(pos, {name = "doorroller:doorroller_"..height.."y_opened",param1="",param2=dir})
		end
		local meta = minetest.get_meta(pos)
		meta:set_string("owner", owner)
		meta:set_string("infotext", doorroller.translate("Roller Door (Owned by '@1')", owner))

		if dir == 0 or dir == 2 then --dir = 0:NORTE 1:OESTE 2:SUL 3:LESTE
			local posLeftNow = {x=pos.x, y=pos.y, z=pos.z}
			node = minetest.get_node(posLeftNow)
			while(node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened")
			do
				posLeftNow = {x=posLeftNow.x-1, y=posLeftNow.y, z=posLeftNow.z}				
				node = minetest.get_node(posLeftNow)
				if node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened" then
					if status == "open" then
						minetest.set_node(posLeftNow, {name = "doorroller:doorroller_"..height.."y_closed",param1="",param2=dir})
					elseif status == "close" then
						minetest.set_node(posLeftNow, {name = "doorroller:doorroller_"..height.."y_opened",param1="",param2=dir})
					end
					local meta = minetest.get_meta(posLeftNow)
					meta:set_string("owner", owner)
					meta:set_string("infotext", doorroller.translate("Roller Door (Owned by '@1')", owner))

				else
					break
				end
			end
			
			local posRightNow = {x=pos.x, y=pos.y, z=pos.z}
			node = minetest.get_node(posRightNow)
			while(node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened")
			do
				posRightNow = {x=posRightNow.x+1, y=posRightNow.y, z=posRightNow.z}				
				node = minetest.get_node(posRightNow)
				if node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened" then
					if status == "open" then
						minetest.set_node(posRightNow, {name = "doorroller:doorroller_"..height.."y_closed",param1="",param2=dir})
					elseif status == "close" then
						minetest.set_node(posRightNow, {name = "doorroller:doorroller_"..height.."y_opened",param1="",param2=dir})
					end
					local meta = minetest.get_meta(posRightNow)
					meta:set_string("owner", owner)
					meta:set_string("infotext", doorroller.translate("Roller Door (Owned by '@1')", owner))
				else
					break
				end
			end
		elseif dir == 1 or dir == 3 then --dir = 0:NORTE 1:OESTE 2:SUL 3:LESTE
			local posLeftNow = {x=pos.x, y=pos.y, z=pos.z}
			node = minetest.get_node(posLeftNow)
			while(node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened")
			do
				posLeftNow = {x=posLeftNow.x, y=posLeftNow.y, z=posLeftNow.z-1}				
				node = minetest.get_node(posLeftNow)
				if node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened" then
					if status == "open" then
						minetest.set_node(posLeftNow, {name = "doorroller:doorroller_"..height.."y_closed",param1="",param2=dir})
					elseif status == "close" then
						minetest.set_node(posLeftNow, {name = "doorroller:doorroller_"..height.."y_opened",param1="",param2=dir})
					end
					local meta = minetest.get_meta(posLeftNow)
					meta:set_string("owner", owner)
					meta:set_string("infotext", doorroller.translate("Roller Door (Owned by '@1')", owner))

				else
					break
				end
			end
			
			local posRightNow = {x=pos.x, y=pos.y, z=pos.z}
			node = minetest.get_node(posRightNow)
			while(node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened")
			do
				posRightNow = {x=posRightNow.x, y=posRightNow.y, z=posRightNow.z+1}				
				node = minetest.get_node(posRightNow)
				if node.name == "doorroller:doorroller_"..height.."y_closed" or node.name == "doorroller:doorroller_"..height.."y_opened" then
					if status == "open" then
						minetest.set_node(posRightNow, {name = "doorroller:doorroller_"..height.."y_closed",param1="",param2=dir})
					elseif status == "close" then
						minetest.set_node(posRightNow, {name = "doorroller:doorroller_"..height.."y_opened",param1="",param2=dir})
					end
					local meta = minetest.get_meta(posRightNow)
					meta:set_string("owner", owner)
					meta:set_string("infotext", doorroller.translate("Roller Door (Owned by '@1')", owner))
				else
					break
				end
			end
		end
	else
		minetest.chat_send_player(username, minetest.colorize("#FF0000", "[ROLLER DOOR]").." "
			..doorroller.translate("You do not own this Roller Door!")
		)
	end
end

doorroller.register_rollerdoor = function(height, recipe)
   if type(height)~="number" then
      height = 3
   end
   if type(recipe)~="table" then
      minetest.log('error',"["..doorroller.modname:upper().."] invalid 'recipe' value!")
   end
   minetest.register_node("doorroller:doorroller_"..height.."y_closed", {
   	description = doorroller.translate("Roller Door (@1 meters)", ("%02d"):format(height)),
   	inventory_image = "icon_door_roller_150x143.png",
   	drop = "doorroller:doorroller_"..height.."y_closed",
   	drawtype = "nodebox",
   	use_texture_alpha = "clip",
   	tiles = doorroller.tiles,
   	sounds = doorroller.sounds.default,
   	--node_box = doorroller.node_format.closed(),
   	--selection_box = doorroller.node_format.closed(),
   	--collisionbox = doorroller.node_format.closed(),
   	node_box = {
   		type = "fixed",
   		fixed = doorroller.node_format.closed(height),
   	},
   	paramtype = "light", --Nao sei pq, mas o blco nao aceita a luz se nao tiver esta propriedade
   	paramtype2 = "facedir",
   	--physical = true, --Se o item dropado colide FISICAMENTE com outros dropados. CUIDADO: pode causar lags principalmente em dispositivos fraco.
   	--collide_with_objects = true, -- Workaround fix for a MT engine bug
   	visual = "wielditem",
   	visual_size = {x=1, y=1}, -- Scale up of nodebox is these * 1.5
   	is_ground_content = true,
   	groups = {cracky=3, stone=1},	
   	on_place = function(itemstack, user, pointed_thing)
   	   return doorroller.on_place(itemstack, user, pointed_thing, height)
   	end,
   	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
   	   return doorroller.on_rightclick(pos, node, clicker, itemstack, pointed_thing, height)
   	end,
   	can_dig = function(pos, player)
   		local isCan = false
   		if player ~= nil and minetest.is_player(player) then
   			isCan = doorroller.canIntect(pos, player:get_player_name())
   		end
   		return isCan
   	end,
   })
   
   minetest.register_node("doorroller:doorroller_"..height.."y_opened", {
   	drop = "doorroller:doorroller_"..height.."y_closed",
   	drawtype = "nodebox",
   	use_texture_alpha = "clip",
   	tiles = doorroller.tiles,
   	--node_box = doorroller.node_format.opened(),
   	--selection_box = doorroller.node_format.opened(),
   	--collisionbox = doorroller.node_format.opened(),
   	drawtype = "nodebox",
   	node_box = {
   		type = "fixed",
   		fixed = doorroller.node_format.opened(height),
   	},
   	paramtype = "light", --Nao sei pq, mas o blco nao aceita a luz se nao tiver esta propriedade
   	paramtype2 = "facedir",
   	--physical = true, --Se o item dropado colide FISICAMENTE com outros dropados. CUIDADO: pode causar lags principalmente em dispositivos fraco.
   	--collide_with_objects = true, -- Workaround fix for a MT engine bug
   	visual = "wielditem",
   	visual_size = {x=1, y=1}, -- Scale up of nodebox is these * 1.5
   	is_ground_content = true,
   	groups = {cracky=3, stone=1, not_in_creative_inventory=1},	
   	on_place = function(itemstack, user, pointed_thing)
   	   return doorroller.on_place(itemstack, user, pointed_thing, height)
   	end,
   	on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
   	   return doorroller.on_rightclick(pos, node, clicker, itemstack, pointed_thing, height)
   	end,
   	can_dig = function(pos, player)
   		--return doorroller.canIntect(pos, player:get_player_name())
   		local isCan = false
   		if player ~= nil and minetest.is_player(player) then
   			isCan = doorroller.canIntect(pos, player:get_player_name())
   		end
   		return isCan
   	end,
   })
   
   minetest.register_craft({
   	output = "doorroller:doorroller_"..height.."y_closed",
   	recipe = recipe
   })
   
   minetest.register_alias("doorroller_"..height.."y", "doorroller:doorroller_"..height.."y_closed")
   minetest.register_alias("rollerdoor_"..height.."y", "doorroller:doorroller_"..height.."y_closed")
   minetest.register_alias("portaoderolo_"..height.."y", "doorroller:doorroller_"..height.."y_closed")
   minetest.register_alias("rolodeporta_"..height.."y", "doorroller:doorroller_"..height.."y_closed")
   minetest.register_alias("portadegaragem_"..height.."y", "doorroller:doorroller_"..height.."y_closed")

end --Final of: doorroller.register_rollerdoor = function(height, recipe)
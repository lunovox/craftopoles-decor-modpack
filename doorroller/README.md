![screenshot]

# Roller Door
[Minetest Mod] 

For Minetest 5.0.0 and later. Compatible with 5.5.1.

### Usage

Adds a track-type garage door, which can only be operated by the owner of the gate.

### Dependencies

* **Mandatory:** [default], [doors] : Mods of Minetest Game.
* **Optional:** 
  * [intllib] : translate to your language. It is not necessary if your engine has the default minetest translator.
  * [tradelands] : Access to interaction with the rolling door is now controlled by the rules of this land protector mod.

### Translated Languages

* **English (Default):** [template.pot], [template.txt]
* **Portuguese:** [pt.po], [doorroller.pt.tr]
* **Brazilian Portuguese:** [pt_BR.po], [doorroller.pt_BR.tr]

To add your language, **[read here]**.

### Change Logs

* **0.2.0 (Beta):** First full version completed. But, it still needs to go through testing to find defects.
* **0.1.0 (Alpha):** It is being developed. It still has many incomplete functions.

### Recipe

![screenshot-recipe-doorroller]

* 4x Steek Ingot
* 2x Steel Block

### Licenses

* Code: [GNU AGPL-3.0]
* Media: [CC BY-SA-4.0]

### Developers

* [Lunovox Heavenfinder]

[screenshot]:https://gitlab.com/lunovox/doorroller/-/raw/main/screenshot.png
[screenshot-recipe-doorroller]:https://gitlab.com/lunovox/doorroller/-/raw/main/screenshot-recipe-doorroller.png
[default]:https://content.minetest.net/metapackages/default/
[doors]:https://content.minetest.net/metapackages/doors/
[intllib]:https://github.com/minetest-mods/intllib
[tradelands]:https://gitlab.com/lunovox/tradelands/
[GNU AGPL-3.0]:https://gitlab.com/lunovox/doorroller/-/raw/main/LICENSE
[CC BY-SA-4.0]:https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR
[Lunovox Heavenfinder]:https://libreplanet.org/wiki/User:Lunovox
[read here]:https://gitlab.com/lunovox/doorroller/-/blob/main/locale/README.md
[template.pot]:https://gitlab.com/lunovox/doorroller/-/blob/main/locale/template.pot
[template.txt]:https://gitlab.com/lunovox/doorroller/-/blob/main/locale/template.txt
[pt.po]:https://gitlab.com/lunovox/doorroller/-/blob/main/locale/pt.po
[doorroller.pt.tr]:https://gitlab.com/lunovox/doorroller/-/blob/main/locale/doorroller.pt.tr
[pt_BR.po]:https://gitlab.com/lunovox/doorroller/-/blob/main/locale/pt_BR.po
[doorroller.pt_BR.tr]:https://gitlab.com/lunovox/doorroller/-/blob/main/locale/doorroller.pt_BR.tr



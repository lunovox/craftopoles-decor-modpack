local ngettext

if minetest.get_modpath("intllib") then
	if intllib.make_gettext_pair then
		-- New method using gettext.
		doorroller.translate, ngettext = intllib.make_gettext_pair()
	else
		-- Old method using text files.
		doorroller.translate = intllib.Getter()
	end
	--minetest.log("warning", "[PORTABLEBOXES] Tradutor 'intllib'!")
elseif minetest.get_translator ~= nil and doorroller.modname ~= nil and doorroller.modname ~= "" and minetest.get_translator(doorroller.modname) then
	doorroller.translate = minetest.get_translator(doorroller.modname)
	--minetest.log("warning", "[PORTABLEBOXES] Tradutor padrão!")
else
	doorroller.translate = function(txt) return txt end
	--minetest.log("warning", "[PORTABLEBOXES] Sem Tradutor!")
end

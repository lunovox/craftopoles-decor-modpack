doorroller={}

doorroller.modname = minetest.get_current_modname()
doorroller.modpath = minetest.get_modpath(doorroller.modname)

dofile(doorroller.modpath.."/translate.lua")
dofile(doorroller.modpath.."/api.lua")

local s0 = ""
local sb = "default:steelblock"
local si = "default:steel_ingot"
doorroller.register_rollerdoor(3, 
   {
		{s0, sb, s0},
		{s0, si, s0},
		{s0, si, s0},
	}
)
doorroller.register_rollerdoor(4, 
   {
		{s0, sb, s0},
		{s0, si, s0},
		{s0, si, si},
	}
)
doorroller.register_rollerdoor(5, 
   {
		{sb, s0, s0},
		{si, s0, s0},
		{si, si, si},
	}
)
doorroller.register_rollerdoor(6, 
   {
		{sb, s0, s0},
		{si, s0, si},
		{si, si, si},
	}
)
doorroller.register_rollerdoor(7, 
   {
		{sb, s0, si},
		{si, s0, si},
		{si, si, si},
	}
)

minetest.log('action',"["..doorroller.modname:upper().."] Loaded!")
